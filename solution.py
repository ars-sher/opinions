# encoding=UTF8

__author__ = 'ars'

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import cross_validation
from sklearn.pipeline import Pipeline
import json
import numpy
import itertools
from nltk.corpus import stopwords
import pandas as pd
import codecs

import re

url = re.compile('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')


class CrossValidation:
    def __init__(self):
        self.solution = Solution()

    def train(self, training_json):
        self.solution = Solution()
        self.solution.train(training_json)

    def test(self, testing_json):
        (corpus, y) = Solution.reviews_from_json_to_corpus_y(testing_json)
        result = self.solution.getClasses(corpus)
        positive = 0
        skipped = 0
        size = len(corpus)

        for res, exp in itertools.izip(result, y):
            if res == exp:
                positive += 1

        precision = float(positive) / (size - skipped)
        recall = float(positive) / size
        F1 = 2 * (precision * recall) / (precision + recall)
        return F1

    def test_common(self, json_data):
        kf = cross_validation.KFold(len(json_data), 10)
        json_data_numpy = numpy.array(json_data)
        F1_list = []
        for train_indices, test_indices in kf:
            self.train(json_data_numpy[train_indices])
            F1_list.append(self.test(json_data_numpy[test_indices]))
        print F1_list
        return 2


class Solution:
    stopWords = stopwords.words('russian')
    # classifier = MultinomialNB()
    # vectorizer = CountVectorizer(ngram_range=(10, 10), stop_words=stopWords)
    classifier = Pipeline(
        [
            ('cv', CountVectorizer(ngram_range=(10, 10), stop_words=stopWords)),
            ('clf', MultinomialNB())
        ]
    )


    # convert text to feature vector (optional)
    @staticmethod
    def feature_extractor(text):
        return Solution.vectorizer.transform(text)

    @staticmethod
    def unite(a, b):
        # currently just finds the intersection
        return [val for val in a if val in b]

    @staticmethod
    def delete_duplicates(training_corpus):
        grouped = itertools.groupby(training_corpus, lambda r: r['text'])
        result = []
        for key, value in grouped:
            v = list(value)
            # print key
            # print v
            united = reduce(
                lambda e1, e2: {"text": e1["text"], "answers": Solution.unite(e1["answers"], e2["answers"])}, v, v[0])
            result.append(united)

        return result
        # with open('jssaved.json', 'w') as outfile:
        # json.dump(training_corpus, outfile, sort_keys=True)

    @staticmethod
    def get_answers(answers):
        return map(lambda rate: (rate["text"], Solution.find_key_by_value(rate, True)), answers)

    # well, I don't like this code
    @staticmethod
    def find_key_by_value(dictionary, value):
        for k, v in dictionary.iteritems():
            if v == value:
                return k

    @staticmethod
    def reviews_from_json_to_corpus_y(reviews_from_json):
        training_corpus = map(lambda item: {u'text': item["text"], u'answers': Solution.get_answers(item["answers"])},
                              reviews_from_json)
        training_corpis_without_dups = Solution.delete_duplicates(training_corpus)
        training_data = [(obj["text"], dict(obj["answers"])[u'Общая характеристика'] if dict(obj["answers"]).get(
            u'Общая характеристика') else "neutral") for obj in training_corpus]
        corpus = map(lambda x: x[0], training_data)
        y = map(lambda x: x[1], training_data)
        return (corpus, y)

    # trainer of classifier (optional)
    def train(self, reviews_from_json):
        # extract text-class pairs from corpus
        (corpus, y) = Solution.reviews_from_json_to_corpus_y(reviews_from_json)
        # X = Solution.vectorizer.fit_transform(corpus)
        self.classifier.fit(corpus, y)

    # returns sentiment score of input text (mandatory)
    def getClasses(self, reviews):
        # feature_vector = Solution.feature_extractor(reviews)
        return self.classifier.predict(reviews)


reviews_from_json = json.load(open(u"reviews.json"))

solution = Solution()
json_for_testing = reviews_from_json[:500]
json_for_learning = reviews_from_json[500:]
# solution.train(json_for_learning)

# (corpus, y) = Solution.reviews_from_json_to_corpus_y(json_for_testing)
# res = solution.getClasses(corpus)
# print expected_results
# print res

# [lr.fit(x[train_indices], y[train_indices]).score(x[test_indices],y[test_indices])
# for train_indices, test_indices in kf_total]

cv = CrossValidation()

cv.train(json_for_learning)
print cv.test(json_for_testing)
# F1 = cv.test_common(reviews_from_json)
# print F1



# print classifier.fit([X[i] for i in a], [y[i] for i in a]).score(X[b], y[b])
